<?php
require_once __DIR__ . '/src/config.php';
require_once __DIR__ . '/vendor/autoload.php';
use SGI\Classes\Template;

Template::header();

?>
<div class="container">
    <h1>Bem vindo ao sistema de notas da Escola Xavier </h1>
    <div class="card">
        <div class="card-header">
            <h3>Opções</h3>
        </div>
        <div class="list-group list-group-flush">
            <div class="list-group-item">
                Cursos:  <a href='curso/principal.php'>Listar</a> | <a href='curso/cadastrar.php'>Cadastrar</a>
            </div>
            <div class="list-group-item">
                Disciplinas:  <a href='disciplina/principal.php'>Listar</a> | <a href='disciplina/cadastrar.php'>Cadastrar</a>
            </div>
            <div class="list-group-item">
                Alunos:  <a href='aluno/principal.php'>Listar</a> | <a href='aluno/cadastrar.php'>Cadastrar</a>
            </div>
        </div>
    </div>
</div>

<?php
Template::footer();
