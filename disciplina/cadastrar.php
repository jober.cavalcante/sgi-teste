<?php
require_once __DIR__ . '/../src/config.php';
require_once ROOT . '/vendor/autoload.php';

use SGI\Classes\Template;

$id = filter_input(INPUT_GET, 'edit', FILTER_VALIDATE_INT);

$disciplinaBase = new SGI\Classes\Disciplinas();
if ($id) {
    $disciplina = $disciplinaBase->get($id);
    if (!$disciplina) {
        Template::header(' - Cadastrar Disciplina');
        SGI\Helpers::errorMessage('Disciplina não disponivel');
    }
} else {
    $disciplina = new stdClass();
}

$cursos = $disciplinaBase->getCursos();

if (!$cursos) {
    Template::header(' - Cadastrar Disciplina');
    SGI\Helpers::errorMessage('Não há Áreas cadastradas');
}


Template::header(' - Cadastrar Disciplina');

?>
<div class="container">
    <?= SGI\Helpers::breadcumb(["<a href='" . SITE_URL . "/disciplina/principal.php'>Disciplinas</a>", 'Cadastrar']); ?>
    <form action="salvar.php" method="post">
        <?php if (!empty($disciplina->id)) : ?>
            <input type="hidden" name="id" value="<?= $disciplina->id ?>">
        <?php endif; ?>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input
                        type="text"
                        class="form-control"
                        id="nome"
                        name="nome" placeholder="Ex.: Aerobiologia"
                        value="<?= !empty($disciplina->nome) ? $disciplina->nome : '' ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="curso_id">Área</label>
                    <?= SGI\Helpers::makeSelect('curso_id', 'curso_id', $cursos, !empty($disciplina->curso_id) ? $disciplina->curso_id : '',  true); ?>
                </div>
            </div>
        </div>
        <button class="btn btn-primary" type="button" id="salvar">Salvar</button>
    </form>
</div>

<?php
Template::footer(['/disciplina/script.js']);
