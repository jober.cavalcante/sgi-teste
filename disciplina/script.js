$(function () {
    $('#salvar').on('click', function () {
        var nome = $('#nome').val().trim();
        var curso = $('#curso_id').val().trim();

        if (!nome.length || !curso) {
            swal.fire(
                    'Atenção',
                    'Você deve Preencher o nome e a área',
                    'warning'
                    ).then(() => {
                setTimeout(() => {
                    $('#nome').focus()
                }, 1000);
            });
            return;
        }

        $.post('/disciplina/salvar.php', $('form').serialize(), function (data) {
            if (data.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Não foi possível salvar',
                });
                return;
            }

            Swal.fire({
                icon: 'success',
                title: 'Atenção',
                text: 'Disciplina salvo',
            }).then(function () {
                window.location.href = "/disciplina/principal.php";
            });
        }, 'json')
        .fail(() => {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Houve uma falha e não foi possível salvar',
            });
        });

    });

})
