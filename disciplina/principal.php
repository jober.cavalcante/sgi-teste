<?php
require_once __DIR__ . '/../src/config.php';
require_once __DIR__ . '/../vendor/autoload.php';

use SGI\Classes\Disciplinas as Disciplinas;
use SGI\Classes\Template;

Template::header();

$disciplinaBase = new Disciplinas();

$disciplinas = $disciplinaBase->getAll();

?>

<div class="container">
    <?= SGI\Helpers::breadcumb(['Disciplinas']); ?>
    <div class="text-right pb-2">
        <a href="/disciplina/cadastrar.php" class="btn btn-secondary">Cadastrar</a>
    </div>
    <?php
    if ($disciplinas) :
        Template::load('disciplina/table_disciplina', ['disciplinas' => $disciplinas]);
    else :

    ?>

    <div class="alert alert-info">Não há áreas cadastradas</div>
    <?php endif; ?>
</div>

<?php
Template::footer();
