<?php
require_once __DIR__ . '/../src/config.php';
require_once __DIR__ . '/../vendor/autoload.php';
//SGI\Helpers::headerJson();


$disciplina = new SGI\Classes\Disciplinas;

$values = new stdClass();
$values->nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
$values->curso_id = filter_input(INPUT_POST, 'curso_id', FILTER_VALIDATE_INT);


$disciplinaId = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);


if ($disciplinaId) {
   $values->id = $disciplinaId;
}

$id = $disciplina->save((array) $values);

if (!$id) {
    exit(json_encode(['error' => 'Não cadastrado']));
}
exit(json_encode(['success' => 'Cadastrado']));
