<?php
require_once __DIR__ . '/../src/config.php';
require_once __DIR__ . '/../vendor/autoload.php';
//SGI\Helpers::headerJson();


$disciplina = new SGI\Classes\Disciplinas();

$disciplinaId = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);

if (!$disciplinaId) {
    exit(json_encode(['error' => 'Disciplina não encontrada']));
}

$disciplina->load($disciplinaId);
if (!$disciplina->info) {
    exit(json_encode(['error' => 'disciplina não encontrada']));
}

if ($disciplina->hasVinculos()) {
    exit(json_encode(['error' => 'disciplina possui alunos']));
}



if (!$disciplina->delete($disciplinaId)) {
    exit(json_encode(['error' => 'Não foi possivel apagar o registro']));
}
exit(json_encode(['success' => 'Removido']));

