<?php
require_once __DIR__ . '/src/config.php';
require_once __DIR__ . '/vendor/autoload.php';
use SGI\Classes\Template;

Template::header();


?>
<div class="container text-center">
    <h1>Pagina não encontrada!</h1>
    <i class="fas fa-wrench fa-6x text-warning"></i>

</div>

<?php
Template::footer();
