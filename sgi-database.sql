-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 17/01/2021 às 19:58
-- Versão do servidor: 10.1.38-MariaDB
-- Versão do PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `sgi`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `alunos`
--

CREATE TABLE `alunos` (
  `id` bigint(11) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `ano_matricula` int(11) NOT NULL,
  `curso_id` bigint(20) NOT NULL,
  `quantidade_disciplinas` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Despejando dados para a tabela `alunos`
--

INSERT INTO `alunos` (`id`, `nome`, `ano_matricula`, `curso_id`, `quantidade_disciplinas`) VALUES
(1, 'Aluno 001', 2010, 9, 3),
(2, 'Aluno 002', 2020, 11, 3),
(3, 'Aluno 003', 2020, 10, 3),
(4, 'aluno 004', 2010, 11, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `cursos`
--

CREATE TABLE `cursos` (
  `id` bigint(20) NOT NULL,
  `nome` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Despejando dados para a tabela `cursos`
--

INSERT INTO `cursos` (`id`, `nome`) VALUES
(9, 'Exatas'),
(10, 'Humanas'),
(11, 'Biológicas');

-- --------------------------------------------------------

--
-- Estrutura para tabela `disciplinas`
--

CREATE TABLE `disciplinas` (
  `id` bigint(20) NOT NULL,
  `nome` varchar(120) NOT NULL,
  `curso_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Despejando dados para a tabela `disciplinas`
--

INSERT INTO `disciplinas` (`id`, `nome`, `curso_id`) VALUES
(1, 'Aerobiologia', 11),
(3, 'Antropologia', 11),
(4, 'Anatomia', 11),
(5, 'Cálculo', 9),
(6, 'Geometria', 9),
(7, 'Trigonometria', 9),
(8, 'Direito Civíl', 10),
(9, 'Psicologia', 10),
(10, 'Filosofia', 10),
(11, 'Teologia', 10),
(12, 'Matemática Financeira', 9),
(13, 'Zootecnia', 11);

-- --------------------------------------------------------

--
-- Estrutura para tabela `disciplinas_alunos`
--

CREATE TABLE `disciplinas_alunos` (
  `id` bigint(20) NOT NULL,
  `disciplina_id` bigint(20) NOT NULL,
  `aluno_id` bigint(20) NOT NULL,
  `nota` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Despejando dados para a tabela `disciplinas_alunos`
--

INSERT INTO `disciplinas_alunos` (`id`, `disciplina_id`, `aluno_id`, `nota`) VALUES
(2, 7, 1, 8),
(8, 8, 3, 7),
(9, 9, 3, 1),
(12, 10, 3, 8),
(13, 5, 1, 9),
(16, 12, 1, 6.5),
(17, 3, 2, 10),
(18, 4, 2, 1),
(19, 1, 2, 9);

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `alunos`
--
ALTER TABLE `alunos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `disciplinas`
--
ALTER TABLE `disciplinas`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `disciplinas_alunos`
--
ALTER TABLE `disciplinas_alunos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `curso_id` (`disciplina_id`,`aluno_id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `alunos`
--
ALTER TABLE `alunos`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `cursos`
--
ALTER TABLE `cursos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `disciplinas`
--
ALTER TABLE `disciplinas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de tabela `disciplinas_alunos`
--
ALTER TABLE `disciplinas_alunos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
