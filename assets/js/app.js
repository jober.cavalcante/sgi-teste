$(function () {

    $('.deleteItem').on('click', function () {
        var _self = $(this);
        Swal.fire({
            title: '<small>Atenção</small>',
            html: `<div class="text-center">Atenção, deseja realmente apagar este item? <br> Ação irreversível</div>`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: `Sim`,
            confirmButtonColor: '#28a745',
            cancelButtonText: `Não`,

        }).then((result) => {
            if (!result.isConfirmed) {
                return;
            }

            $.post(_self.data('url'), {id: _self.data('id')}, function (data) {

                if (data.error) {
                    Swal.fire({
                        icon: 'warning',
                        text: 'Erro ao deletar: ' + data.error,
                    }).then(function () {
                        document.location.reload(true);
                    });

                    return;
                }

                Swal.fire({
                    icon: 'success',
                    title: 'Registro deletado!',
                }).then(function () {
                    document.location.reload(true);
                });

            }, 'json')
                    .fail(() => {
                        swal.fire('Atenção', 'Houve um erro inesperado!', 'error');
                    })
        });


    });
})
