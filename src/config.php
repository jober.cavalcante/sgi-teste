<?php
date_default_timezone_set('America/Manaus');
ini_set('session.gc_maxlifetime', 4 * 3600);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


$dbConfig = [
    'driver' => 'mysql',
    'charset' => 'utf8',
    'host' => 'localhost',
    'dbname' => 'SGI',
    'user' => 'root',
    'password' => '',
    'fetchMode' => PDO::FETCH_OBJ, // Default
];
define('SITE_URL', 'http://localhost:8080');



define('ROOT', dirname(__FILE__, 2));
define('TEMPLATE_DIR', ROOT . '/template');

define('SCRIPT_VERSION', '0.006');
define('STYLE_VERSION', '0.004');

