<?php
namespace SGI\Classes;

class Model
{
    public $table = null;
    public $DB;


    public function __construct()
    {
        global $dbConfig;
        $this->DB = new \QueryBuilder\Database($dbConfig);
    }


    public function get(int $id)
    {
        $result = $this->DB
            ->select('*')
            ->from($this->table)
            ->where('id', '=', $id)
            ->execute()
            ->fetch();

        return $result;
    }

    public function getAll($fetchAll = true)
    {
        $result = $this->DB
                ->select('*')
                ->from($this->table)
                ->order('id')
                ->execute();

        return $fetchAll ? $result->fetchAll(): $result;
    }

    public function save($values)
    {
        if (!empty($values['id'])) {
            $id = $values['id'];
            unset($values['id']);
            return $this->update($values, $id)? $id : false;
        }


        return $this->insert($values);
    }

    public function insert($values)
    {
        $insert = $this->DB->insert($this->table)->values($values);
        if (!$insert->execute()) {
            return false;
        }

        $consulta = $this->DB->select('id')
            ->from($this->table);

        foreach ($values as $key => $value) {
            $consulta->where($key, '=', $value);
        }

        $consulta->limit(1)
            ->order('id', 'desc');


        return $consulta->execute()->fetch()->id;
    }

    public function update($values, int $id)
    {
        $update = $this->DB->update($this->table)
            ->values($values)
            ->where('id', '=', $id);
        return $update->execute();
    }

    public function delete(int $id)
    {
        return $this
            ->DB
            ->delete($this->table)
            ->where('id', '=', $id)
            ->execute();
    }

}
