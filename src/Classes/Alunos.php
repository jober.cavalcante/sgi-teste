<?php
namespace SGI\Classes;

use \SGI\Classes\Cursos as Cursos;

class Alunos extends Model
{

    public $table = 'alunos';

    public function getCursos()
    {
        $cursoBase = new Cursos();
        return $cursoBase->getAll();
    }

    public function getAll($fetchAll = true)
    {
        $result = $this->DB
            ->select('al.*, a.nome as curso')
            ->from($this->table . ' as al')
            ->join('cursos as a')
            ->on('a.id', '=', 'al.curso_id')
            ->execute();

        return $fetchAll ? $result->fetchAll() : $result;
    }

    public function getAllDisciplinas($alunoId)
    {
        return $this->DB
                ->select('c.*')
                ->from('disciplinas_alunos ca')
                ->join('disciplinas c')
                ->on('c.id', '=', 'ca.disciplina_id')
                ->where('ca.aluno_id', '=', $alunoId)
                ->execute()->fetchAll();
    }

   

    public function getAllDisciplinasDisponiveis($aluno, $disciplinas)
    {
        $ids = [];

        if (!empty($disciplinas)) {
            foreach ($disciplinas as $disciplina) {
                $ids[] = $disciplina->id;
            }
        }

        $query = $this->DB
                ->select('*')
                ->from('disciplinas')
                ->where('curso_id', '=', $aluno->curso_id);

        if($ids) {
            $query->where('id', 'NOT IN', $ids);
        }

        return $query->execute()->fetchAll();
    }
}
