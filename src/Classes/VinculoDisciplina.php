<?php
namespace SGI\Classes;

class VinculoDisciplina extends Model
{
    public $table = 'disciplinas_alunos';

    public function getByUserAndDisciplina($alunoId, $disciplinaId)
    {
        $result = $this->DB
            ->select('*')
            ->from($this->table)
            ->where('aluno_id', '=', $alunoId)
            ->where('disciplina_id', '=', $disciplinaId)
            ->execute()
            ->fetch()->id;

        return $result;
    }

}
