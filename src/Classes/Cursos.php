<?php
namespace SGI\Classes;

class Cursos extends Model
{
    public $table = 'cursos';
    public $info;


    public function load($id)
    {
        $this->info = $this->get($id);
    }

    public function hasVinculos()
    {
        $result = $this->DB
                ->select('count(*) as total')
                ->from('disciplinas')
                ->where('curso_id', '=', $this->info->id);

        return $result->execute()->fetch()->total;
    }
}
