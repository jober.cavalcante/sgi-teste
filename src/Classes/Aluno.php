<?php
namespace SGI\Classes;

class Aluno
{

    public $info;
    private $DB;

    public function __construct($dados)
    {
        global $dbConfig;

        $this->info = $dados;

        $this->DB = new \QueryBuilder\Database($dbConfig);
    }

    public function getInfo()
    {
        return $this->info;
    }

    public function insertVinculo($values)
    {
        $model = new \SGI\Classes\Model();
        $model->table = 'disciplinas_alunos';
        $id = $model->insert((array) $values);

        if ($id) {
            $this->increaseDisciplinas();
        }

        return $id;
    }

    private function increaseDisciplinas()
    {
        $result = $this->DB
            ->update('alunos')
            ->values('quantidade_disciplinas', new \QueryBuilder\Builder\Clause\Expr('quantidade_disciplinas + 1'))
            ->where('id', '=', $this->info->id)
            ->execute();

        return;
    }

    public function getSituacao()
    {
        $disciplinas = $this->getAllDisciplinas();

        if (!$disciplinas) {
            return 'Não há disciplinas';
        }

        if ( $this->info->quantidade_disciplinas < 3) {
            return 'Não atingiu o minimo de 3 disciplinas matriculadas';
        }

        $notas = [];
        $disciplinasNotasNaoLancadas = [];
        foreach ($disciplinas as $disciplina) {
            if(!is_numeric($disciplina->nota)) {
                $disciplinasNotasNaoLancadas[] = $disciplina->nome;
                continue;
            }
            $notas[] =  $disciplina->nota;
        }

        if (empty($notas)) {
            return 'Notas não lançadas';
        }

        if (count($notas) < $this->info->quantidade_disciplinas) {
            return 'Há disciplinas com notas não lançadas: '. implode(', ', $disciplinasNotasNaoLancadas);
        }

        $media = array_sum($notas) / $this->info->quantidade_disciplinas;

        $this->info->media = number_format($media, 2);

        return $this->getSituacaoMedia();
    }

    private function getSituacaoMedia()
    {

        return $this->info->media < 7 ? '<span class="text-danger">Reprovado</span>' : '<span class="text-success">Aprovado</span>';
    }

    public function deleteVinculo($disciplinaId)
    {
        $id = $this->DB
                ->select('*')
                ->from('disciplinas_alunos')
                ->where('aluno_id', '=', $this->info->id)
                ->where('disciplina_id', '=', $disciplinaId)
                ->execute()
                ->fetch()->id;

        if (!$id) {
            return false;
        }

        $result = $this->DB
            ->delete('disciplinas_alunos')
            ->where('id', '=', $id)
            ->execute();

        if ($result) {
            $this->decreaseDisciplinas();
        }

        return $result;
    }


    public function updateNota($nota, $disciplina)
    {
         $result = $this->DB
            ->update('disciplinas_alunos')
            ->values('nota', $nota)
            ->where('aluno_id', '=', $this->info->id)
            ->where('disciplina_id', '=', $disciplina)
            ->execute();

         return $result;
    }



    private function decreaseDisciplinas()
    {
        $result = $this->DB
            ->update('alunos')
            ->values('quantidade_disciplinas', new \QueryBuilder\Builder\Clause\Expr('quantidade_disciplinas - 1'))
            ->where('id', '=', $this->info->id)
            ->execute();

        return $result;
    }

    public function getAllDisciplinas()
    {
        return $this->DB
                ->select('c.*, ca.nota')
                ->from('disciplinas_alunos ca')
                ->join('disciplinas c')
                ->on('c.id', '=', 'ca.disciplina_id')
                ->where('ca.aluno_id', '=', $this->info->id)
                ->execute()->fetchAll();
    }

    public function getAllDisciplinasDisponiveis($disciplinas)
    {
        $ids = [];

        if (!empty($disciplinas)) {
            foreach ($disciplinas as $disciplina) {
                $ids[] = $disciplina->id;
            }
        }

        $query = $this->DB
            ->select('*')
            ->from('disciplinas')
            ->where('curso_id', '=', $this->info->curso_id);

        if ($ids) {
            $query->where('id', 'NOT IN', $ids);
        }

        return $query->execute()->fetchAll();
    }
}
