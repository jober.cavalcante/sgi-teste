<?php
namespace SGI\Classes;


class Template
{
    private $templateDir;

    function __construct()
    {
        global $templateDir;
        $this->templateDir =  TEMPLATE_DIR;
    }

    public static function header($name = '', array $styles = [])
    {

        include_once TEMPLATE_DIR.'/header.php';
    }


    public static function footer(array $scripts = [])
    {
        if($scripts) {
            foreach ($scripts as $script) {
                echo "<script src='{$script}?v=".SCRIPT_VERSION."'></script>\n";
            }
        }
        include_once TEMPLATE_DIR.'/footer.php';
    }

    public static function load(string $template, array $variables = [])
    {

        extract($variables);
        include TEMPLATE_DIR.'/'.$template.'.php';
    }
}
