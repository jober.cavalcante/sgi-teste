<?php
namespace SGI\Classes;

use \SGI\Classes\Cursos as Cursos;
class Disciplinas extends Model
{

    public $table = 'disciplinas';

    public $info;

    public function load($id)
    {
        $this->info = $this->get($id);
    }

    public function hasVinculos()
    {
        $result = $this->DB
                ->select('count(*) as total')
                ->from('disciplinas_alunos')
                ->where('disciplina_id', '=', $this->info->id);

        return $result->execute()->fetch()->total;
    }


    public function getCursos()
    {
        $cursoBase = new Cursos();
        return $cursoBase->getAll();

    }

    public function getAll($fetchAll = true)
    {
        $result = $this->DB
                ->select('d.*, c.nome as curso')
                ->from($this->table . ' as d')
                ->join('cursos as c')
                ->on('c.id', '=', 'd.curso_id')
                ->execute();

        return $fetchAll ? $result->fetchAll(): $result;
    }
}
