<?php
namespace SGI;

class Helpers
{

    public static function headerJson()
    {
        header('Content-Type: application/json; charset=utf-8');
    }


    public static function makeEditButton(string $url, string $title)
    {
        return '<a title="' . $title . '" href="' . SITE_URL . $url . '"><i class="fas fa-edit fa-fw"></i></a>';
    }

    public static function makeDeleteButton(string $url, int $id, string $title)
    {
        return '<span class="deleteItem" title="' . $title . '" data-id="' . $id . '" data-url="' . SITE_URL . $url . '"><i class="far fa-trash-alt fa-fw pointer"></i></span>';
    }

    public static function makeViewButton(string $url, string $title)
    {
        return '<a title="' . $title . '" href="' . SITE_URL . $url . '"><i class="fas fa-desktop fa-fw"></i></a>';
    }

    public static function makeSelect($name, $id, $data, $selectedRaw = '', $isRequired = true)
    {
        $options = ['<option value="">Escolha</option>'];
        foreach ($data as $option) {
            $selected = $selectedRaw == $option->id ? 'selected' : '';
            $options[] = "<option {$selected} value='{$option->id}' >{$option->nome}</option>";
        }

        $required = $isRequired ? 'required' : '';

        return "<select {$required} class='form-control' name='{$name}' id='{$id}'>" . implode("\n", $options) . "</select>";
    }

    public static function redirect($url)
    {
        global $SITE_URL;

        $fullUrl = $SITE_URL . $url;

        header("Location: {$fullUrl}");
        exit;
    }

    public static function errorMessage($message, string $url = null)
    {
        $button = '';

        $button = "<a href='" . SITE_URL . "' class='btn btn-outline-danger'>Retornar a página principal</a>";

        if (filter_var($url, FILTER_VALIDATE_URL)) {
            $button = "<a href='{$url}' class='btn btn-outline-danger'>Retornar a página principal</a>";
        }

        echo "<div class='container'>
                <div class='alert alert-danger text-center'>
                <p>{$message}</p><br>
                    {$button}
                </div>
                </div>";
        \SGI\Classes\Template::footer();
        exit;
    }

    public static function formatDate($date)
    {
        try {
            $formatedDate = new \DateTime($date);
            return $formatedDate->format('d/m/Y');
        } catch (\Exception $ex) {
            return '';
        }
    }

    public static function breadcumb(array $items)
    {
        $navItem = ['<li class="breadcrumb-item"><a href="' . SITE_URL . '">Início</a></li>'];
        foreach ($items as $item) {
            $navItem[] = '<li class="breadcrumb-item">' . $item . '</li>';
        }
        return '<nav aria-label="breadcrumb"><ol class="breadcrumb">' . implode(' ', $navItem) . '</ol></nav>';
    }

    public static function cleanString($text)
    {
        $utf8 = array(
            '/[áàâãªä]/u' => 'a',
            '/[ÁÀÂÃÄ]/u' => 'A',
            '/[ÍÌÎÏ]/u' => 'I',
            '/[íìîï]/u' => 'i',
            '/[éèêë]/u' => 'e',
            '/[ÉÈÊË]/u' => 'E',
            '/[óòôõºö]/u' => 'o',
            '/[ÓÒÔÕÖ]/u' => 'O',
            '/[úùûü]/u' => 'u',
            '/[ÚÙÛÜ]/u' => 'U',
            '/ç/' => 'c',
            '/Ç/' => 'C',
            '/ñ/' => 'n',
            '/Ñ/' => 'N',
            '/–/' => '-', // UTF-8 hyphen to "normal" hyphen
            '/[’‘‹›‚]/u' => '', // Literally a single quote
            '/[“”«»„]/u' => '', // Double quote
            '/ /' => '_', // nonbreaking space (equiv. to 0x160)
            '/:/' => '-',
        );
        return preg_replace(array_keys($utf8), array_values($utf8), $text);
    }
}
