<div class="table-responsive">
    <table class="table table-bordered">
        <thead class="thead-dark">
            <tr>
                <th scope="col">id</th>
                <th scope="col">Disciplina</th>
                <th scope="col">Curso</th>
                <th scope="col"> - </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($disciplinas as $disciplina): ?>
                <tr>
                    <td><?= $disciplina->id ?></td>
                    <td><?= $disciplina->nome ?></td>
                    <td><?= $disciplina->curso ?></td>
                    <td class="text-center">
                        <?= SGI\Helpers::makeEditButton("/disciplina/cadastrar.php?edit={$disciplina->id}", 'Editar Disciplina') ?>
                        <?= SGI\Helpers::makeDeleteButton("/disciplina/delete.php", $disciplina->id, 'Deletar Disciplina') ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
