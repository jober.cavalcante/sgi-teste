<div class="table-responsive">
    <table class="table table-bordered">
        <thead class="thead-dark">
            <tr>
                <th scope="col">id</th>
                <th scope="col">Curso</th>
                <th scope="col"> - </th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cursos as $curso): ?>
                <tr>
                    <td><?= $curso->id ?></td>
                    <td><?= $curso->nome ?></td>
                    <td class="text-center">
                        <?= SGI\Helpers::makeEditButton("/curso/cadastrar.php?edit={$curso->id}", 'Editar Curso') ?>
                        <?= SGI\Helpers::makeDeleteButton("/curso/delete.php", $curso->id, 'Editar Curso') ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
