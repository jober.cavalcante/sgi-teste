<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <title>SGI <?= $name ?></title>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="/assets/bootstrap/dist/css/bootstrap.min.css">
        <link href="/assets/fontawesome/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="/assets/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css"/>
        <link rel="stylesheet" href="/assets/style.css"/>

        <script src="/assets/jquery/dist/jquery.min.js"></script>
        <script src="https://getbootstrap.com/docs/4.1/assets/js/vendor/popper.min.js"></script>
        <script src="/assets/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/assets/js/app.js"></script>
        <?php
        if ($styles):
            foreach ($styles as $style):
                $link = filter_var($style, FILTER_VALIDATE_URL) ?: SITE_URL."/assets/$style";
                ?>
                <link rel="stylesheet" href="<?=$link?>">
                <?php
            endforeach;
        endif;

        ?>

    </head>
</head>
<body>
    <div  class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4  bg-white border-bottom shadow-sm fixed-top ">
        <h5 class="my-0 mr-md-auto font-weight-normal">
            SGI
        </h5>
    </div>
    <div class="mb-5">&nbsp;</div>
    
