
<footer class="footer ">
    <div class="text-center">
        <small>Desenvolvido por Jober Cavalcante</small>
    </div>
</footer>
<script src="/assets/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
<script src="/assets/sweetalert2/dist/sweetalert2.all.min.js"></script>
<script src="/assets/js/app.js?v=<?= SCRIPT_VERSION ?>"></script>
</body>
</html>
