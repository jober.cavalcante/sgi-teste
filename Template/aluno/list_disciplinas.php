<div class="card" id="disciplinasAtivas">
    <div class="card-header">
        <h5>Disciplinas</h5>
    </div>
    <ul class="list-group list-group-flush <?= $disciplinas ? '' : 'd-none' ?>">
        <?php if ($disciplinas): ?>
            <?php
            foreach ($disciplinas as $disciplina) :
                $nota = $disciplina->nota? number_format($disciplina->nota, 2): '';
                ?>
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-8"><?= $disciplina->nome ?></div>
                        <div class="col-2 text-right nota">
                            <?= $nota ?>
                        </div>
                        <div class="col-2 text-center">
                            <i
                                data-id="<?= $disciplina->id ?>"
                                data-nome="<?= $disciplina->nome ?>"
                                data-nota="<?= $nota ?>"
                                title="Lançar Nota"
                                class="fas fa-fw fa-calculator text-success lancaNota pointer"></i>
                            <i
                                data-id="<?= $disciplina->id ?>"
                                data-nome="<?= $disciplina->nome ?>"
                                title="Remover disciplina"
                                class="fas fa-fw fa-minus text-danger removerDisciplina pointer"></i>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
    <div class="card-body <?= !$disciplinas ? '' : 'd-none' ?>">
        <div class="alert alert-info">Não possui disciplinas!</div>
    </div>
</div>
