<div class="table-responsive">
    <table class="table table-bordered">
        <thead class="thead-dark">
            <tr class="">
                <th scope="col" style="width: 3%">id</th>
                <th scope="col" style="width: 40%">Aluno</th>
                <th scope="col" style="width: 5%">Ano matricula</th>
                <th scope="col">Curso</th>
                <th scope="col">Situação</th>
                <th scope="col" class="text-center" style="width: 10%"> - </th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($alunos as $alunoData):
                $aluno = new SGI\Classes\Aluno($alunoData);
            ?>
                <tr>
                    <td><?= $aluno->info->id ?></td>
                    <td><?= $aluno->info->nome ?></td>
                    <td><?= $aluno->info->ano_matricula ?></td>
                    <td><?= $aluno->info->curso ?></td>
                    <td><?= $aluno->getSituacao() ?></td>
                    <td class="text-center">

                        <?= SGI\Helpers::makeViewButton("/aluno/ver.php?id={$aluno->info->id}", 'Ver Aluno') ?>
                        <?= SGI\Helpers::makeEditButton("/aluno/cadastrar.php?edit={$aluno->info->id}", 'Editar Aluno') ?>
                        <?= SGI\Helpers::makeDeleteButton("/aluno/delete.php", $aluno->info->id, 'Deletar Aluno') ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
