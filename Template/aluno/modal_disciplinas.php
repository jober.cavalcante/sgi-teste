<!-- Button trigger modal -->
<div class="text-right">
    <button type="button" class="btn btn-link btn-sm mt-3" data-toggle="modal" data-target="#disciplinasModal">
        Vincular disciplina
    </button>
</div>

<!-- Modal -->
<div class="modal fade" id="disciplinasModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="disciplinasModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="disciplinasModalLabel">Disciplinas disponíveis</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <?php if ($disciplinasDisponiveis) : ?>
                    <ul class="list-group">
                        <?php foreach ($disciplinasDisponiveis as $disciplina) : ?>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-10"><?= $disciplina->nome ?></div>
                                    <div class="col-2 text-right">
                                        <i
                                            data-id="<?= $disciplina->id ?>"
                                            data-nome="<?= $disciplina->nome ?>"
                                            class="fas fa-plus adicionarDisciplina pointer"></i>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php else: ?>
                    <div class="alert alert-info">Não há disciplinas disponiveis</div>
                <?php endif; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>


<form class="modal fade" id="lancarNotaModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="lancarNotaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="lancarNotaModallLabel">
                    Lançar nota:
                    <span id="lancarNotaDisciplinaNome"></span>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <label for="notaDisciplina">
                            Digite a nota
                            <i  class="far fa-question-circle text-warning" data-toggle="tooltip" data-placement="top" title="A nota deve conter as casas decimais"></i>
                        </label>
                        <input placeholder="ex.: 10.00" type="text" class="form-control" id="notaDisciplina" aria-describedby="notaDisciplinaHelp">
                        <small id="notaDisciplinaHelp" class="form-text text-muted">A nota deve ser de zero a dez.</small>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" id='salvarNota' class="btn btn-primary">Salvar</button>
            </div>
        </div>
    </div>
</form>
