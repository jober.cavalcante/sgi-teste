<?php
require_once __DIR__ . '/../src/config.php';
require_once __DIR__ . '/../vendor/autoload.php';
//SGI\Helpers::headerJson();


$curso = new SGI\Classes\Cursos();

$cursoId = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);

if (!$cursoId) {
    exit(json_encode(['error' => 'Curso não encontrada']));
}

$curso->load($cursoId);
if (!$curso->info) {
    exit(json_encode(['error' => 'Curso não encontrado']));
}

if ($curso->hasVinculos()) {
    exit(json_encode(['error' => 'Curso possui disciplinas']));
}


if (!$curso->delete($cursoId)) {
    exit(json_encode(['error' => 'Não foi possivel apagar o registro']));
}
exit(json_encode(['success' => 'Cadastrado']));

