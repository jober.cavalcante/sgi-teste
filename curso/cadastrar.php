<?php
require_once __DIR__ . '/../src/config.php';
require_once ROOT . '/vendor/autoload.php';

use SGI\Classes\Template;

$id = filter_input(INPUT_GET, 'edit', FILTER_VALIDATE_INT);

if ($id) {
    $cursoBase = new SGI\Classes\Cursos();
    $curso = $cursoBase->get($id);
    if (!$curso) {
        Template::header(' - Cadastrar Curso');
        SGI\Helpers::errorMessage('Curso não disponivel');
    }
} else {
    $curso = new stdClass();
}
Template::header(' - Cadastrar Curso');

?>
<div class="container">
    <?= SGI\Helpers::breadcumb(["<a href='" . SITE_URL . "/curso/principal.php'>Cursos</a>", 'Cadastrar']); ?>
    <form action="salvar.php" method="post">
        <?php if (!empty($curso->id)) : ?>
            <input type="hidden" name="id" value="<?= $curso->id ?>">
        <?php endif; ?>
        <div class="form-group">
            <label for="name">Nome</label>
            <input
                type="text"
                class="form-control"
                id="nome"
                name="nome" placeholder="Ex.: Exatas"
                value="<?= !empty($curso->nome) ? $curso->nome : '' ?>">
        </div>
        <button class="btn btn-primary" type="button" id="salvar">Salvar</button>
    </form>
</div>

<?php
Template::footer(['/curso/script.js']);
