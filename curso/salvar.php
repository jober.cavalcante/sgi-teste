<?php
require_once __DIR__ . '/../src/config.php';
require_once __DIR__ . '/../vendor/autoload.php';
//SGI\Helpers::headerJson();


$curso = new SGI\Classes\Cursos();

$values = new stdClass();
$values->nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);


$cursoId = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);

if ($cursoId) {
   $values->id = $cursoId;
}

$id = $curso->save((array) $values);

if (!$id) {
    exit(json_encode(['error' => 'Não cadastrado']));
}
exit(json_encode(['success' => 'Cadastrado']));
