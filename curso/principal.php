<?php
require_once __DIR__ . '/../src/config.php';
require_once __DIR__ . '/../vendor/autoload.php';

use SGI\Classes\Cursos as Curso;
use SGI\Classes\Template;

Template::header();

$cursoBase = new Curso();

$cursos = $cursoBase->getAll();

?>

<div class="container">
    <?= SGI\Helpers::breadcumb(['Cursos']); ?>
    <div class="text-right pb-2">
        <a href="/curso/cadastrar.php" class="btn btn-secondary">Cadastrar</a>
    </div>
    <?php
    if ($cursos) :
        Template::load('curso/table_curso', ['cursos' => $cursos]);
    else :

        ?>

        <div class="alert alert-info">Não há cursos cadastradas</div>
<?php endif; ?>

</div>

<?php
Template::footer();
