$(function () {
    $('#salvar').on('click', function () {
        var nome = $('#nome').val().trim();

        if (!nome.length) {
            swal.fire(
                    'Atenção',
                    'Você deve Preencher o nome ',
                    'warning'
                    ).then(() => {
                setTimeout(() => {
                    $('#nome').focus()
                }, 1000);
            });
            return;
        }

        $.post('/curso/salvar.php', $('form').serialize(), function (data) {
            if (data.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Não foi possível salvar',
                });
                return;
            }

            Swal.fire({
                icon: 'success',
                title: 'Atenção',
                text: 'Curso salva',
            }).then(function () {
                window.location.href = "/curso/principal.php";
            });
        }, 'json')
        .fail(() => {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Houve uma falha e não foi possível salvar',
            });
        });

    });

})
