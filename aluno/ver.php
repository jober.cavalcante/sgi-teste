<?php
require_once __DIR__ . '/../src/config.php';
require_once ROOT . '/vendor/autoload.php';

use SGI\Classes\Template;

$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

$alunoBase = new SGI\Classes\Alunos();
if (!$id) {
    Template::header(' - Ver Aluno');
    SGI\Helpers::errorMessage('Id não informado');
}
$alunoData = $alunoBase->get($id);
if (!$alunoData) {
    Template::header(' - Cadastrar Aluno');
    SGI\Helpers::errorMessage('Aluno não disponivel');
}

$aluno = new SGI\Classes\Aluno($alunoData);



$cursoBase = new \SGI\Classes\Cursos();
$curso = $cursoBase->get($aluno->info->curso_id);

if (!$curso) {
    Template::header(' - Visualizar Aluno');
    SGI\Helpers::errorMessage('Não há Cursos cadastradas');
}

$disciplinas = $aluno->getAllDisciplinas();

Template::header(' - Visualizar Aluno');
$disciplinasDisponiveis = $aluno->getAllDisciplinasDisponiveis($disciplinas);


$situacao = $aluno->getSituacao();
?>
<div class="container">
    <?= SGI\Helpers::breadcumb(["<a href='" . SITE_URL . "/aluno/principal.php'>Alunos</a>", 'visualizar']); ?>
    <div>
        <div class="card mb-3" id="aluno" data-id="<?=$aluno->info->id?>">
            <div class="row no-gutters">
                <div class="col-md-3 text-center">
                    <img class="img-fluid" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRs8GV0srGn2-orwBYbj-EbZ1LW5P0AlaO6Ag&usqp=CAU" alt="">
                </div>
                <div class="col-md-9">
                    <div class="card-body">
                        <h5 class="card-title"><?= $aluno->info->nome ?></h5>
                        <p class="card-text"><b>Curso: </b><?= $curso->nome ?></p>
                        <p class="card-text"><b>Ano de matricula: </b><?= $aluno->info->ano_matricula ?></p>
                        <p class="card-text <?=$situacao? '': 'd-none'?>">
                            <b>Situação: </b><?= $aluno->getSituacao() ?>
                        </p>
                        <?= Template::load('aluno/list_disciplinas', ['disciplinas' => $disciplinas]); ?>
                        <?= Template::load('aluno/modal_disciplinas', ['disciplinasDisponiveis' => $disciplinasDisponiveis]); ?>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>
<?php
Template::footer(['/aluno/script.js']);

