<?php
require_once __DIR__ . '/../src/config.php';
require_once __DIR__ . '/../vendor/autoload.php';
SGI\Helpers::headerJson();


$aluno = new SGI\Classes\Alunos();

$values = new stdClass();
$values->nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
$values->curso_id = filter_input(INPUT_POST, 'curso_id', FILTER_VALIDATE_INT);
$values->ano_matricula = filter_input(INPUT_POST, 'ano_matricula', FILTER_VALIDATE_INT);

if (!$values->nome || !$values->curso_id || !$values->ano_matricula) {
   exit(json_encode(['error' => 'Favor preencher todos os dados']));
}


$alunoId = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);

if ($alunoId) {
   $values->id = $alunoId;
}

$id = $aluno->save((array) $values);

if (!$id) {
    exit(json_encode(['error' => 'Não cadastrado']));
}
exit(json_encode(['success' => $id]));
