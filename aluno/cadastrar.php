<?php
require_once __DIR__ . '/../src/config.php';
require_once ROOT . '/vendor/autoload.php';

use SGI\Classes\Template;

$id = filter_input(INPUT_GET, 'edit', FILTER_VALIDATE_INT);

$alunoBase = new SGI\Classes\Alunos();
if ($id) {
    $aluno = $alunoBase->get($id);
    if (!$aluno) {
        Template::header(' - Cadastrar Aluno');
        SGI\Helpers::errorMessage('Aluno não disponivel');
    }
} else {
    $aluno = new stdClass();
}

$cursos = $alunoBase->getCursos();

if (!$cursos) {
    Template::header(' - Cadastrar Aluno');
    SGI\Helpers::errorMessage('Não há Cursos cadastradas');
}

Template::header(' - Cadastrar Aluno');

?>
<div class="container">
    <?= SGI\Helpers::breadcumb(["<a href='" . SITE_URL . "/aluno/principal.php'>Alunos</a>", 'Cadastrar']); ?>
    <form action="salvar.php" method="post">
        <?php if (!empty($aluno->id)) : ?>
            <input type="hidden" name="id" value="<?= $aluno->id ?>">
        <?php endif; ?>
        <div class="row">
            <div class="col">
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input
                        type="text"
                        class="form-control"
                        id="nome"
                        name="nome" placeholder="Ex.: Aluno 001"
                        value="<?= !empty($aluno->nome) ? $aluno->nome : '' ?>">
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-3">
                <div class="form-group">
                    <label for="ano_matricula">Ano Matricula</label>
                    <input class="form-control"
                           name="ano_matricula"
                           id="ano_matricula"
                           type="text"
                           value="<?= !empty($aluno->ano_matricula) ? $aluno->ano_matricula : '' ?>" />
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="curso_id">Curso</label>
                    <?= SGI\Helpers::makeSelect('curso_id', 'curso_id', $cursos, !empty($aluno->curso_id) ? $aluno->curso_id : '', true); ?>
                </div>
            </div>
        </div>
        <button class="btn btn-primary" type="button" id="salvar">Salvar</button>
    </form>
</div>

<?php
Template::footer(['/assets/jquery-mask-plugin/dist/jquery.mask.min.js', '/aluno/script.js']);
