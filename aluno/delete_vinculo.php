<?php
require_once __DIR__ . '/../src/config.php';
require_once __DIR__ . '/../vendor/autoload.php';
//SGI\Helpers::headerJson();

$values = new stdClass();
$values->aluno_id = filter_input(INPUT_POST, 'aluno', FILTER_VALIDATE_INT);
$values->disciplina_id = filter_input(INPUT_POST, 'disciplina', FILTER_VALIDATE_INT);


if (!$values->aluno_id || !$values->disciplina_id) {
    exit(json_encode(['error' => 'Dados inválidos']));
}

$alunoBase = new SGI\Classes\Alunos();
$alunoData = $alunoBase->get($values->aluno_id);

if (!$alunoData) {
    exit(json_encode(['error' => 'Aluno não encontrado']));
}

$aluno = new SGI\Classes\Aluno($alunoData);

$vinculo = $aluno->deleteVinculo($values->disciplina_id);


if (!$vinculo) {
    exit(json_encode(['error' => 'Não foi possivel apagar o registro']));
}
exit(json_encode(['success' => 'Removido']));
