<?php
require_once __DIR__ . '/../src/config.php';
require_once __DIR__ . '/../vendor/autoload.php';

use SGI\Classes\Alunos as Alunos;
use SGI\Classes\Template;

Template::header();

$alunoBase = new Alunos();

$alunos = $alunoBase->getAll();

?>

<div class="container">
    <?= SGI\Helpers::breadcumb(['aluno']); ?>
    <div class="text-right pb-2">
        <a href="/aluno/cadastrar.php" class="btn btn-secondary">Cadastrar</a>
    </div>
    <?php
    if ($alunos) :
        Template::load('aluno/table_aluno', ['alunos' => $alunos]);
    else :

    ?>

    <div class="alert alert-info">Não há alunos cadastradas</div>
    <?php endif; ?>
</div>

<?php
Template::footer();
