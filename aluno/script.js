$(function () {

    $('#ano_matricula').mask("0000", {clearIfNotMatch: true});
    $('#notaDisciplina').mask("09.99", {clearIfNotMatch: true});

    var disciplinasModal = $('#disciplinasModal');
    var aluno = $('#aluno');
    var disciplinasAtivas = $('#disciplinasAtivas');
    var lancarNotaModal = $('#lancarNotaModal');
    var notaDisciplina = $('#notaDisciplina');

    $('[data-toggle="tooltip"]').tooltip()


    disciplinasAtivas.on('click', '.removerDisciplina', function () {
        const disciplina = $(this);

        console.log(disciplina.data());
        Swal.fire({
            title: '<small>Atenção</small>',
            html: `<div class="text-center">Remover a disciplina: ${disciplina.data('nome')}?<br><small>A nota serão removidas!</small></div>`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: `Sim`,
            confirmButtonColor: '#28a745',
            cancelButtonText: `Não`,

        }).then((result) => {
            if (!result.isConfirmed) {
                return;
            }

            $.post('/aluno/delete_vinculo.php',
                    {aluno: aluno.data('id'), disciplina: disciplina.data('id')},
                    function (data) {
                        if (data.error) {
                            swal.fire('Atenção', 'Erro ao remover, recarregue a página', 'error');
                            return
                        }

                        document.location.reload(true);
                    }, 'json')
                    .fail(() => {
                        swal.fire('ola mundo');
                    });
        });
    })

    var disciplinaAtual = null;

    disciplinasAtivas.on('click', '.lancaNota ', function () {
        const disciplina = $(this);
        const dadosDisciplina = disciplina.data();
        notaDisciplina.val('');
        console.log(dadosDisciplina);
        disciplinaAtual = dadosDisciplina;
        notaDisciplina.val(dadosDisciplina.nota ? dadosDisciplina.nota : '');
        lancarNotaModal.find('#lancarNotaDisciplinaNome').html(dadosDisciplina.nome)
        lancarNotaModal.modal('show');
    });

    $('#salvarNota').on('click', function () {
        let _self = $(this);
        _self.attr('disabled', true);

        let dados = {
            nota: $('#notaDisciplina').val(),
            aluno: aluno.data('id'),
            disciplina: disciplinaAtual.id
        }


        if (!dados.nota.length) {
            swal.fire('Atenção', 'Você só pode salvar com a nota preenchida', 'error');
            _self.attr('disabled', false);
            return;
        }

        if (dados.nota > 10) {
            swal.fire('Oops.', 'A nota máxima permitida é 10', 'error');
            _self.attr('disabled', false);
            return;
        }
        if (dados.nota < 0) {
            swal.fire('Oops.', 'A nota mínima permitida é 0', 'error');
            _self.attr('disabled', false);
            return;
        }

        $.post('/aluno/salvar_nota.php', dados, function (data) {
            if (data.error) {
                swal.fire('Atenção', 'Erro ao remover, recarregue a página', 'error');
                return
            }

            document.location.reload(true);

        }, 'json')
                .fail(() => {
                    swal.fire('Erro inexperado ao salvar nota, tente recarregar a página');
                })

        _self.attr('disabled', false);
    })


    disciplinasModal.on('click', '.adicionarDisciplina', function () {
        const disciplina = $(this);

        console.log(disciplina.data());
        Swal.fire({
            title: '<small>Atenção</small>',
            html: `<div class="text-center">Adicionar a disciplina: ${disciplina.data('nome')}?</div>`,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: `Sim`,
            confirmButtonColor: '#28a745',
            cancelButtonText: `Não`,

        }).then((result) => {
            if (!result.isConfirmed) {
                return;
            }

            $.post('/aluno/vincular_disciplina.php',
                    {aluno: aluno.data('id'), disciplina: disciplina.data('id')},
                    function (data) {
                        if (data.error) {
                            swal.fire('Atenção', 'Erro ao salvar, recarregue a página', 'error');
                            return
                        }

                        disciplina.parents('li').remove();
                        adicionarVinculo(disciplina.data());

                    }, 'json')
                    .fail(() => {
                        swal.fire('Erro inexperado');
                    });
        });

    });

    disciplinasModal.on('hidden.bs.modal', function (e) {
        document.location.reload(true);
    })

    function adicionarVinculo(disciplina) {
        disciplinasAtivas.find('.card-body').addClass('d-none');
        disciplinasAtivas.find('.list-group').removeClass('d-none').append(makeLiDisciplina(disciplina));
    }

    function makeLiDisciplina(disciplina) {

        return `<li class="list-group-item">
                    <div class="row">
                        <div class="col-8">${disciplina.nome}</div>
                        <div class="col-2 text-right"></div>
                        <div class="col-2 text-center">
                            <i
                                data-id="${disciplina.id}" data-nome="${disciplina.nome}"
                                title="Lançar Nota"
                                class="fas fa-fw fa-calculator text-success lancaNota pointer"></i>
                            <i
                                data-id="${disciplina.id}" data-nome="${disciplina.nome}"
                                title="Remover disciplina"
                                class="fas fa-fw fa-minus text-danger removerDisciplina pointer"></i>
                        </div>
                    </div>
                </li>`;
    }




    $('#salvar').on('click', function () {
        var nome = $('#nome').val().trim();
        var curso = $('#curso_id').val().trim();
        var ano_matricula = $('#ano_matricula').val().trim();



        if (!nome.length || !curso || !ano_matricula) {
            swal.fire(
                    'Atenção',
                    'Você deve Preencher o nome, ano de matricula    e curso',
                    'warning'
                    ).then(() => {
                setTimeout(() => {
                    $('#nome').focus()
                }, 1000);
            });
            return;
        }

        $.post('/aluno/salvar.php', $('form').serialize(), function (data) {
            if (data.error) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Não foi possível salvar',
                });
                return;
            }

            Swal.fire({
                icon: 'success',
                title: 'Atenção',
                text: 'Aluno salvo',
            }).then(function () {
                window.location.href = "/aluno/ver.php?id=" + data.success;
            });
        }, 'json')
                .fail(() => {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Houve uma falha e não foi possível salvar',
                    });
                });

    });

})
